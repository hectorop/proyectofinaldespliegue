package fp.daw.despliegue;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/confirmacion")
public class ConfirmacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource dataSource;

	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
			if (dataSource == null)
				throw new ServletException("DataSource desconocido");
		} catch (NamingException e) {
			Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cc = request.getParameter("cc");
		if (cc != null) {
			/* se reciben datos del formulario */ enviarRespuesta(response, confirmar(cc));
		} else {
			/* no se reciben datos de confirmación: se redirecciona a la página de inicio */ response
					.sendRedirect("inicio");
		}

	}

	private String confirmar(String cc) {
		
		/*
		 * Parámetro cc: recibe el código de confirmación
		 *
		 * retorna: null si se completa la confirmaicón con exito, en caso contrario
		 * retorna un mensaje de error.
		 *
		 * Realiza la confirmación con los datos almacenados en la base de datos.
		 *
		 */
		return cc;
	}

	private void enviarRespuesta(HttpServletResponse response, String confirmado) throws IOException {
		/*
		 * Parámetro confimado: recibe true si la confirmación ha tenido exito, false en
		 * caso contrario
		 *
		 *
		 * Completa este método para enviar el contenido web que corresponda en función
		 * del valor del parámetro confirmado.
		 *
		 */
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			/* aquí la sentencias que generan el contenido web */

		} finally {
			if (out != null)
				out.close();
		}
	}

}
