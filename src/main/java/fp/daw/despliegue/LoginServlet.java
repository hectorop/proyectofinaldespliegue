package fp.daw.despliegue;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource dataSource;

	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
			if (dataSource == null)
				throw new ServletException("DataSource desconocido");
		} catch (NamingException e) {
			Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session != null) {
			/* la sesión está iniciada */
			response.sendRedirect("login");
		} else {
			/* no se ha iniciado sesión */
			String id = request.getParameter("id");
			String password = request.getParameter("password");
			if (id != null && password != null) {
				/* se reciben datos del formulario */
				String estado = validar(id, password);
				/* String estado = null; */
				if (estado == null) {
					/* credenciales válidas: se inicia la sesión */
					request.getSession().setAttribute("usuario", id);
					response.sendRedirect("inicio");
				} else {
					/* credenciales de inicio de sesión no válidas o error interno */
					LoginRegistroHTML.enviar(response, estado, id, false);
				}

			} else {
				/* no se reciben datos del formulario: se envía el formulario */
				LoginRegistroHTML.enviar(response, null, null, false);
			}
		}
	}

	private String validar(String id, String password) throws IOException, ServletException {

		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sql = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			sql = String.format("select password from usuarios where id='%s'", id);
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				String hash = resultSet.getString("password");
				if (HashPassword.checkBase64(password, hash)) {
					return null;
				} else {
					return "usuario no registrado o contraseña incorrecta";
				}
			} else {
				return "usuario no registrado o contraseña incorrecta";
			}
		} catch (SQLException | NoSuchElementException e) {
			Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, e);
			return "error interno, contacte con el administrador";
		} finally {
			if (resultSet != null)
				try {
					resultSet.close();
				} catch (SQLException e1) {
				}
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e1) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
				}
		}

	}

}