package fp.daw.despliegue;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/registro")
public class RegistroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource dataSource;

	@Override
	public void init(ServletConfig config) throws ServletException {
		try {
			InitialContext context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
			if (dataSource == null)
				throw new ServletException("DataSource desconocido");
		} catch (NamingException e) {
			Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if (session != null) {
			/* la sesión está iniciada */
			response.sendRedirect("inicio");
		} else {
			/* no se ha iniciado sesión */
			String id = request.getParameter("id");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			if (id != null && email != null && password != null) {
				/* se reciben datos del formulario */
				StringBuffer url = request.getRequestURL();
				url.replace(url.length() - 8, url.length(), "confirmar?cc=");
				String estado = registroUsuario(url, id, email, password);
				if (estado == null) {
					/* usuario registrado con exito */
					request.getRequestDispatcher("postregistro").forward(request, response);
				} else {
					/* datos no válidos o error interno */
					LoginRegistroHTML.enviar(response, estado, id, true);
				}
			} else {
				/* no se reciben datos del formulario: se envía el formulario */
				LoginRegistroHTML.enviar(response, null, null, true);
			}
		}
	}

	protected String registroUsuario(StringBuffer url, String id, String email, String password) throws IOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] hash = messageDigest.digest((new Date().toString() + email).getBytes());
			String cc = Base64.getUrlEncoder().encodeToString(Arrays.copyOf(hash, 33));
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			String sql = String.format("select * from usuarios where id='%s'", id);
			if (statement.executeQuery(sql).next())
				return "el nombre de usuario ya ha sido registrado";
			sql = String.format("select * from usuarios where email='%s'", email);
			if (statement.executeQuery(sql).next())
				return "la dirección de correo ya ha sido registrada";
			sql = String.format("insert into usuarios values ('%s', '%s', '%s', '%s')", id, email,
					HashPassword.hashBase64(password), cc);
			statement.executeUpdate(sql);
			url.append(cc);
			enviarCorreo(email, url.toString());
			return null;
		} catch (SQLException | NoSuchAlgorithmException | MessagingException e) {
			Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
			return "error interno, contacte con el administrador";
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e1) {
				}
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e1) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
				}
		}
	}

	private void enviarCorreo(String to, String url) throws MessagingException {
		StringBuilder cuerpo = new StringBuilder("<h1>Confirma tu eMail</h1>");
		cuerpo.append("<p>Para tener acceso a tu nueva cuenta de usuario necesitas ");
		cuerpo.append("confirmar tu eMail. Pulsa el botón confirmar para completar ");
		cuerpo.append("el registro.</p>");
		cuerpo.append(String.format("<p><a href=\"%s\">Confirmar</a>", url));
		Properties properties = new Properties();
// Nombre del host de correo (smtp.gmail.com para enviar a través de gmail)
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
// TLS si está disponible 
		properties.setProperty("mail.smtp.starttls.enable", "true");
// Puerto de gmail para envío de correos 
		properties.setProperty("mail.smtp.port", "587");
// Si requiere o no usuario y password para conectarse. 
		properties.setProperty("mail.smtp.auth", "true");
		Session mailSession = Session.getInstance(properties);
// Para obtener un log de salida más extenso 
		mailSession.setDebug(true);
		Message message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress("alumnofpdaw@gmail.com"));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject("Confirmación de Registro");
		message.setContent(cuerpo.toString(), "text/html");
		message.setSentDate(new Date());
		Transport.send(message, "alumnofpdaw@gmail.com", "practicas2020");
	}
}